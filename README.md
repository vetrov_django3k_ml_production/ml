# ml

# Requirements
- 10 GB free space

# How to run project

- `python3 -m venv venv`
- `pip3 install -r requirements.txt`
- `flask run --debug`

# Problem solving

In case you got such a problem:

```text
Traceback (most recent call last):
  File "/opt/project/test_script.py", line 1, in <module>
    import yolov5
  File "/usr/local/lib/python3.10/site-packages/yolov5/__init__.py", line 1, in <module>
    from yolov5.helpers import YOLOv5
  File "/usr/local/lib/python3.10/site-packages/yolov5/helpers.py", line 4, in <module>
    from yolov5.models.common import AutoShape, DetectMultiBackend
  File "/usr/local/lib/python3.10/site-packages/yolov5/models/common.py", line 18, in <module>
    import cv2
  File "/usr/local/lib/python3.10/site-packages/cv2/__init__.py", line 181, in <module>
    bootstrap()
  File "/usr/local/lib/python3.10/site-packages/cv2/__init__.py", line 153, in bootstrap
    native_module = importlib.import_module("cv2")
  File "/usr/local/lib/python3.10/importlib/__init__.py", line 126, in import_module
    return _bootstrap._gcd_import(name[level:], package, level)
ImportError: libGL.so.1: cannot open shared object file: No such file or directory
```

Go to environment settings

File --> Settings --> Project: ml --> Python Interpreter --> Show all

Then to Interpreter path, click plus button and add path to cv2 lib folder

![screenshot](imgs/troubleshooting.png)


