import base64
import os

import cv2
from flask import Flask, request, jsonify

from model import model_init

app = Flask(__name__)
app.config["UPLOAD_FOLDER"] = "uploads"

# model init
model = model_init()


def process_request(images):
    result = model(images)
    return result


def process_response_ml(results):
    labels_counting = {}
    for i, (im, pred) in enumerate(zip(results.ims, results.pred)):
        labels_counting[i] = {}
        if pred.shape[0]:
            for c in pred[:, -1].unique():
                n = (pred[:, -1] == c).sum()
                labels_counting[i][results.names[int(c)]] = int(n)
    results.save(save_dir='results/', exist_ok=True)
    return labels_counting


def remove_dir_files(path):
    if os.path.exists(path):
        files = os.listdir(path)
        for file in files:
            os.remove(os.path.join(path, file))
    else:
        os.makedirs(path, exist_ok=True)


def remove_temp_data():
    remove_dir_files(app.config['UPLOAD_FOLDER'])
    remove_dir_files('results')


@app.route('/detect', methods=['POST'])
def detect_objects():
    # removing temp data
    remove_temp_data()

    # getting images from request
    images = request.files.getlist('uploaded_files')
    for idx, photo in enumerate(images):
        photo.save(os.path.join(app.config["UPLOAD_FOLDER"], str(idx) + '.jpg'))

    # loading and detecting images
    images = [cv2.imread(os.path.join(app.config["UPLOAD_FOLDER"], file)) for file in
              os.listdir(app.config["UPLOAD_FOLDER"])]
    result = process_request(images)

    # processing images and labels
    labels_counting = process_response_ml(result)

    # encoding result images
    encoded_result_images = []
    for file in sorted(os.listdir('results/'), key=lambda x: int(x.strip('.jpg').strip('image'))):
        result_image_path = os.path.join('results/', file)
        with open(result_image_path, "rb") as f:
            encoded_result_image = base64.b64encode(f.read()).decode('utf-8')
            encoded_result_images.append(encoded_result_image)

    return jsonify({'images': encoded_result_images, 'labels': labels_counting})


if __name__ == '__main__':
    app.run(debug=True, port=5000)
